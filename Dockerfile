FROM rabbitmq:3-management

WORKDIR /SnakeMQ

COPY src/ /SnakeMQ

EXPOSE 15672
EXPOSE 5672