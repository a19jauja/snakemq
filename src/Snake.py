import pygame
import _thread
from rabbitHelper import send, receive
from Moves import Moves

class Snake:
    def __init__(self, surface, init_x, init_y, size):
        self.surface = surface
        self.x = init_x
        self.y = init_y
        self.size = size
        self.step = size
        self.length = 1
        self.pos_rect = [self.get_rectangle()]
        self.draw()
        _thread.start_new_thread(receive, ('events', self.updatePos))
        _thread.start_new_thread(receive, ('blockCaught', self.growSnake))

    def draw(self):
        if self.length <= len(self.pos_rect):
            pygame.draw.rect(self.surface, (255, 255, 255), self.pos_rect.pop(0))
        self.pos_rect.append(self.get_rectangle())
        if self.length > 1:
            pygame.draw.rect(self.surface, (0, 0, 0), self.pos_rect[-2])
        pygame.draw.rect(self.surface, (255, 0, 255), self.pos_rect[-1])

    def get_rectangle(self):
        return pygame.Rect(self.x - self.size / 2, 
            self.y - self.size / 2,
            self.size,
            self.size)

    def isGameOver(self):
        w, h = self.surface.get_size()
        frameCond = self.x + self.step < w and self.x - self.step > 0 and self.y - self.step > 0 and self.y + self.step < h
        for rect in self.pos_rect:
            if rect.x + self.size / 2 == self.x and rect.y + self.size / 2 == self.y and frameCond:
                send('gameOver', 'Game Over !')

    def updatePos(self, ch, method, properties, body):
        intMessage = int(body)
        w, h = self.surface.get_size()
        if intMessage == Moves.MOVE_RIGHT and self.x + self.step < w:
            self.x += self.step
        if intMessage == Moves.MOVE_LEFT and self.x - self.step > 0:
            self.x -= self.step
        if intMessage == Moves.MOVE_UP and self.y - self.step > 0:
            self.y -= self.step
        if intMessage == Moves.MOVE_DOWN and self.y + self.step < h:
            self.y += self.step
        send('headPos', str(self.x) + '/' + str(self.y))
        self.isGameOver()
        self.draw()
    
    def growSnake(self, ch, method, properties, body):
        self.length += 1