import pygame
import _thread
from time import sleep
from rabbitHelper import send, receive
from Snake import Snake
from Moves import Moves
from Block import Block

pygame.init()

wWindow = 800
hWindow = 600
wSnake = 20
eventMessage = -1
gameOn = True

def createNewBlock(ch, method, properties, body):
    global block
    block = Block(window, wSnake)

def listenGameOver(ch, method, properties, body):
    global gameOn
    gameOn = False
    print(body)

window = pygame.display.set_mode((wWindow, hWindow))
pygame.display.set_caption('SnakeMQ')
window.fill((255, 255, 255))
snake = Snake(window, wWindow/2,hWindow/2, wSnake)
block = Block(window, wSnake)
pygame.display.update()

_thread.start_new_thread(receive, ('blockCaught', createNewBlock))
_thread.start_new_thread(receive, ('gameOver', listenGameOver))

while gameOn:
    event = pygame.event.poll()
    if event.type == pygame.QUIT:
        break
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_RIGHT:
            eventMessage = Moves.MOVE_RIGHT
        if event.key == pygame.K_LEFT:
            eventMessage = Moves.MOVE_LEFT
        if event.key == pygame.K_UP:
            eventMessage = Moves.MOVE_UP
        if event.key == pygame.K_DOWN:
            eventMessage = Moves.MOVE_DOWN
    if eventMessage != -1:
        send('events', int(eventMessage))
    pygame.display.update()
    sleep(0.04)

print("Score :", snake.length)
pygame.quit()
