# SnakeMQ

Jeu de Snake implémenté à l'aide de RabbitMQ dans le cadre de l'UE CALC.

## Exécution du code

Pour exécuter SnakeMQ, il est nécessaire d'avoir au préalable installé Docker Desktop : RabbitMQ est implémenté pour être exécuté dans un conteneur Docker. Le jeu étant implémenté en python, il est nécessaire de disposer d'une version de python au moins aussi récente que Python 3.8.

Si Docker est bien installé, la procédure est simple :
* Ouvrir un terminal dans le dossier du projet,
* Exécuter la commande `./launch.sh`,
* Exécuter la commande `python3 src/main.py` pour lancer le jeu.